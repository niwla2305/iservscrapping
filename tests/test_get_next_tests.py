"""
The MIT License (MIT)
Copyright (c) Alwin Lohrie (Niwla23)
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
"""

import unittest
import os
from dotenv import load_dotenv
from iservscrapping import Iserv


load_dotenv()


class GetNextTestsTest(unittest.TestCase):
    """contains tests for getting classtests"""

    def setUp(self) -> None:
        self.iserv = Iserv(os.environ["BASE_URL"],
                           os.environ["USERNAME"], os.environ["PASSWORD"])
        self.iserv.login()
        return super().setUp()

    def test_get(self):
        """tests wheter data can be fetched and looks correct"""
        result = self.iserv.get_next_tests()
        for test in result:
            self.assertIsNotNone(test.date)
            self.assertIsNotNone(test.time)
            self.assertIsNotNone(test.course)
            self.assertIsNotNone(test.name)
            self.assertIn(test.kind, ["Klausur", "Termin"])


if __name__ == '__main__':
    unittest.main()
