"""
The MIT License (MIT)
Copyright (c) Alwin Lohrie (Niwla23)
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
"""
import os
import unittest
from dotenv import load_dotenv
from iservscrapping import untis_scrapper


load_dotenv()


class UntisScrapperTest(unittest.TestCase):
    def test_html1(self):
        with open(os.path.dirname(os.path.realpath(__file__))+"/untisplan_test.htm", "r") as file:
            scrapper = untis_scrapper.UntisScrapper(file.read())

        result = scrapper.scrape()
        self.assertEqual(result.week, "B")
        self.assertEqual(result.date, "14.12.2021")
        print(result.meta_table.blocked_rooms)
        self.assertEqual(len(result.courses), 50)

        self.assertEqual(len(result.meta_table.teachers_absent), 32)
        self.assertEqual(len(result.meta_table.courses_absent), 104)
        self.assertEqual(len(result.meta_table.blocked_rooms), 23)
        self.assertEqual(len(result.meta_table.affected_teachers), 44)
        self.assertEqual(len(result.meta_table.affected_courses), 32)
        self.assertEqual(len(result.meta_table.affected_rooms), 30)


if __name__ == '__main__':
    unittest.main()
