Welcome to iservscrapping's documentation!
==========================================
Scrapper Classes
------------------------------------------

.. autoclass:: iservscrapping.base.BaseIserv
    :members:

.. autoclass:: iservscrapping.Iserv
    :members:

.. autoclass:: iservscrapping.task.Task
    :members:

UntisScrapper
------------------------------------------

.. autoclass:: iservscrapping.untis_scrapper.UntisScrapper
    :members:

Exceptions
------------------------------------------
.. autoclass:: iservscrapping.errors.IservError
    :members:

.. autoclass:: iservscrapping.errors.LoginError
    :members:

.. autoclass:: iservscrapping.errors.WrongContentError
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Fake Classes
------------------------------------------
Fake classes are classes that look just like actual scrapper classes,
but do not make any requests to the server. All data returned is either
static or random.

.. autoclass:: iservscrapping.fake.FakeBaseIserv
    :members:

.. autoclass:: iservscrapping.fake.FakeIserv
    :members:

.. autoclass:: iservscrapping.fake.FakeTask
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`